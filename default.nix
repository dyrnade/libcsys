{ stdenv, lib, fetchFromGitLab, udisks2, qt6, cmake, ninja }:

stdenv.mkDerivation rec {
  pname = "libcsys";
  version = "4.3.0";

#  src = fetchFromGitLab {
#    owner = "cubocore";
#    repo = pname;
#    rev = "v${version}";
#    sha256 = "sha256-/iRFppe08+rMQNFjWSyxo3Noy0iNaelg0JAczg/BYBs=";
#  };

  src = ./.;

  nativeBuildInputs = [
    cmake
    ninja
  ];

  dontWrapQtApps = true;
  buildInputs = [
    qt6.qtbase
    udisks2
  ];
  cmakeFlags = [ "-DUSE_QT6=ON" ];
  meta = with lib; {
    description = "Library for managing drive and getting system resource information in real time";
    homepage = "https://gitlab.com/cubocore/libcsys";
    license = licenses.gpl3Plus;
    maintainers = with maintainers; [ dan4ik605743 ];
    platforms = platforms.linux;
  };
}
