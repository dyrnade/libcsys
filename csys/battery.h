/*
  *
  * This file is a part of Libcsys.
  * Library for getting system resource information in real time.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  *
  */


#pragma once

#include "libcsys_global.h"

class QDBusInterface;
class QDBusConnection;
class QSettings;

class Battery;
typedef QList<Battery> Batteries;

class LIBCSYSSHARED_EXPORT BatteryManager {
public:
	BatteryManager();

	Batteries batteries();

	void refreshBatteries();

private:
	Batteries mBatts;
};

class Battery {
public:
	Battery(const QString&);
	Battery(const Battery&);
	~Battery();

	QString name();

	QStringList properties();
	QVariant value(const QString &);

	bool isValid() const;

private:
	QStringList mProperties;
	QDBusInterface *iface;
};
