/*
  * This file is a part of LibCSys.
  * Library for getting system resource information in real time.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  *
  */


#include <QFileInfo>
#include <QIODevice>
#include <QtGlobal>

#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
#include <QRegularExpression>
#endif
#include <QXmlStreamReader>

#include "storageinfo.h"

#define DBUS_SERVICE    "org.freedesktop.UDisks2"
#define DBUS_PATH       "/org/freedesktop/UDisks2/block_devices/"

/*
  *
  * StorageManager - That manages all the storage volumes
  *
  */

StorageManager::StorageManager() : QObject()
{
	devWatch = new QFileSystemWatcher(this);

	if (not devWatch->addPath("/dev/disk/by-id/"))
	{
		qCritical() << "Failed to add /dev/disk/by-id/ to QFileSystemWatcher. Auto device detection disabled";
	}

	connect(devWatch, SIGNAL(directoryChanged(QString)), this, SLOT(detectChanges()));

	rescanStorage();
}


StorageDevices StorageManager::devices()
{
	return mDevices;
}


StorageBlocks StorageManager::blocks()
{
	return mBlocks;
}


StorageBlocks StorageManager::validBlocks()
{
	return mValidBlocks;
}


void StorageManager::rescanStorage()
{
	mDevices.clear();
	mBlocks.clear();
	mValidBlocks.clear();

	mDeviceNames.clear();
	mBlockNames.clear();

	QDBusInterface iface(DBUS_SERVICE, "/org/freedesktop/UDisks2/block_devices", "org.freedesktop.DBus.Introspectable", QDBusConnection::systemBus());

	if (not iface.isValid())
	{
		return;
	}

	QDBusReply<QString> reply = iface.call("Introspect");

	if (not reply.isValid())
	{
		return;
	}

	QXmlStreamReader xml(reply.value());

	while (!xml.atEnd())
	{
		xml.readNext();

		if ((xml.tokenType() == QXmlStreamReader::StartElement) && (xml.name().toString() == "node"))
		{
			QString blockID = xml.attributes().value("name").toString();

			if (blockID.isEmpty())
			{
				continue;
			}

			/* StorageBlock */
			StorageBlock block(blockID);

			/* DriveID: TSSTcorp...., WDC-WD1..., JetFlash-Trans.... etc... */
			QString driveID = QFileInfo(block.drive()).baseName();

			/* If this block is already added, move on */
			if (not mBlockNames.contains(blockID))
			{
				/* Add the device ID to a list */
				mBlockNames << blockID;
				mBlocks << block;

				/* Valid FS for a block device; not excluded */
				if (block.fileSystem().count() and block.fileSystem() != "swap")
				{
					mValidBlocks << block;
				}
			}

			/* If driveID is empty, do not proceed */
			if (not driveID.isEmpty() and not mDeviceNames.contains(driveID))
			{
				mDeviceNames << driveID;
				mDevices << StorageDevice(driveID, this);
			}
		}
	}
}


void StorageManager::detectChanges()
{
	QSet<QString> oldDevs(mBlockNames.begin(), mBlockNames.end());

	rescanStorage();
	QSet<QString> newDevs(mBlockNames.begin(), mBlockNames.end());

	Q_FOREACH (QString dev, QSet<QString>(newDevs).subtract(oldDevs))
	{
		emit deviceAdded("/org/freedesktop/UDisks2/block_devices/" + dev);
	}

	Q_FOREACH (QString dev, QSet<QString>(oldDevs).subtract(newDevs))
	{
		emit deviceRemoved("/org/freedesktop/UDisks2/block_devices/" + dev);
	}
}


/*
  *
  * StorageDevice - Class to access the physical devices
  *
  */

StorageDevice::StorageDevice(StorageManager *sMgr)
{
	mIsRemovable  = false;
	mIsOptical    = false;
	mSize         = 0;
	mRotationRate = 0;
	mSMgr = sMgr;
}


StorageDevice::StorageDevice(const QString &driveID, StorageManager *sMgr) : mDriveId(driveID)
{
	iface = new QDBusInterface(DBUS_SERVICE, "/org/freedesktop/UDisks2/drives/" + driveID, QString("%1.Drive").arg(DBUS_SERVICE), QDBusConnection::systemBus());
	if (not iface->isValid())
	{
		return;
	}

	mLabel        = property("Vendor").toString() + " " + property("Model").toString();
	mPath         = iface->path();
	mId           = property("Id").toString();
	mIsRemovable  = property("Removable").toBool();
	mIsOptical    = (property("MediaCompatibility").toStringList().filter("optical").count() > 0);
	mSize         = property("Size").toULongLong();
	mRotationRate = property("RotationRate").toInt();
	mSeat         = property("Seat").toString();
	mSMgr = sMgr;
}


StorageDevice::StorageDevice(const StorageDevice &other)
	: mDriveId(other.mDriveId)
{
	iface = new QDBusInterface(DBUS_SERVICE, "/org/freedesktop/UDisks2/drives/" + mDriveId, QString("%1.Drive").arg(DBUS_SERVICE), QDBusConnection::systemBus());
	if (!iface->isValid()) {
		return;
	}

	mLabel = other.mLabel;
	mPath = other.mPath;
	mId = other.mId;
	mIsRemovable = other.mIsRemovable;
	mIsOptical = other.mIsOptical;
	mSize = other.mSize;
	mRotationRate = other.mRotationRate;
	mSeat = other.mSeat;
	mSMgr = other.mSMgr;
}


StorageDevice::~StorageDevice()
{
	delete iface;
}


StorageBlocks StorageDevice::partitions()
{
	if (mParts.isEmpty())
	{
		readPartitions();
	}

	return mParts;
}


StorageBlocks StorageDevice::validPartitions()
{
	if (mValidParts.isEmpty())
	{
		readPartitions();
	}

	return mValidParts;
}


QString StorageDevice::label()
{
	return mLabel;
}


QString StorageDevice::path()
{
	return mPath;
}


QString StorageDevice::id()
{
	return mId;
}


bool StorageDevice::isRemovable()
{
	return mIsRemovable;
}


bool StorageDevice::isOptical()
{
	return mIsOptical;
}


quint64 StorageDevice::size()
{
	return mSize;
}


int StorageDevice::rotationRate()
{
	return mRotationRate;
}


QString StorageDevice::seat()
{
	return mSeat;
}


QVariant StorageDevice::property(const QString &key)
{
	if (not iface)
	{
		return QVariant();
	}
	return iface->property(key.toLatin1().constData());
}

void StorageDevice::readPartitions()
{
    mParts.clear();
	mPartNames.clear();
	mValidParts.clear();

	Q_FOREACH (StorageBlock block, mSMgr->blocks())
	{
		if (QFileInfo(block.drive()).baseName() == mDriveId)
		{
			if (not mPartNames.contains(block.path()))
			{
				mParts << block;
				mPartNames << block.path();

				if (block.fileSystem().count() and block.fileSystem() != "swap")
				{
					mValidParts << block;
				}
			}
		}
	}
}


StorageBlock::StorageBlock()
{
	mIsOptical     = false;
	mIsRemovable   = false;
	mAvailableSize = 0;
	mTotalSize     = 1;
}


StorageBlock::StorageBlock(const QString &id)
	: mPath(DBUS_PATH + id)
	, mDevice("/dev/" + id)
{
	getMountPoint();

	QDBusInterface blockIFace(DBUS_SERVICE, mPath, QString("%1.Block").arg(DBUS_SERVICE), QDBusConnection::systemBus());
	QDBusInterface partIFace(DBUS_SERVICE, mPath, QString("%1.Partition").arg(DBUS_SERVICE), QDBusConnection::systemBus());

	if (not (blockIFace.isValid() and partIFace.isValid()))
	{
		return;
	}

	QString name = blockIFace.property("IdLabel").toString().simplified();

	/* If we don't get the name from IdLabel */
	if (not name.count())
	{
		name = QFileInfo(mMountPoint).baseName();
	}

	/* If the device is not mounted */
	if (not name.count())
	{
		name = id;
	}

	mLabel = QString(name);

	mDrive      = blockIFace.property("Drive").value<QDBusObjectPath>().path();
	mFileSystem = blockIFace.property("IdType").toString();

	QDBusInterface driveIFace(DBUS_SERVICE, mDrive, QString("%1.Drive").arg(DBUS_SERVICE), QDBusConnection::systemBus());

	if (not driveIFace.isValid())
	{
		return;
	}

	QStringList compat = driveIFace.property("MediaCompatibility").toStringList();

	mIsOptical = compat.filter("optical_").count() > 0;

	mIsRemovable = driveIFace.property("Removable").toBool();
	mTotalSize   = partIFace.property("Size").toULongLong();

	mAvailableSize = 0;
	if (not mMountPoint.isEmpty())
	{
		mAvailableSize = QStorageInfo(mMountPoint).bytesAvailable();
	}
}


void StorageBlock::getMountPoint()
{
	QFile dev("/etc/mtab");

	dev.open(QFile::ReadOnly);

	QStringList mounts = QString::fromLocal8Bit(dev.readAll()).split("\n", Qt::SkipEmptyParts);

	Q_FOREACH (QString mount, mounts)
	{
		if (mount.startsWith(mDevice + " "))
		{
		#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
			mMountPoint = mount.split(QRegularExpression("\\s"), Qt::SkipEmptyParts)[1];
		#else
            mMountPoint = mount.split(QRegExp("\\s"), Qt::SkipEmptyParts)[1];
		#endif
			break;
		}
	}
}


QString StorageBlock::label()
{
	return mLabel;
}


QString StorageBlock::path()
{
	return mPath;
}


QString StorageBlock::device()
{
	return mDevice;
}


QString StorageBlock::drive()
{
	return mDrive;
}


QString StorageBlock::mountPoint()
{
	return mMountPoint;
}


QString StorageBlock::fileSystem()
{
	return mFileSystem;
}


bool StorageBlock::isOptical()
{
	return mIsOptical;
}


bool StorageBlock::isRemovable()
{
	return mIsRemovable;
}


quint64 StorageBlock::totalSize()
{
	return mTotalSize;
}


quint64 StorageBlock::availableSize()
{
	return mAvailableSize;
}


QVariant StorageBlock::property(const QString &interface, const QString &key)
{
	QDBusInterface iface(DBUS_SERVICE, mPath, QString(DBUS_SERVICE) + "." + interface, QDBusConnection::systemBus());

	if (not iface.isValid())
	{
		qCritical() << QObject::tr("Failed D-Bus connection.");
		return QVariant();
	}

	return iface.property(key.toLocal8Bit().constData());
}


bool StorageBlock::mount()
{
	if (not mIsOptical)
	{
		QDBusInterface filesystem(DBUS_SERVICE, mPath, QString("%1.Filesystem").arg(DBUS_SERVICE), QDBusConnection::systemBus());

		if (!filesystem.isValid())
		{
			qCritical() << QObject::tr("Failed D-Bus connection.");
			return false;
		}

		QVariantMap options;

		if (mFileSystem == "vfat")
		{
			options.insert("options", "flush");
		}

		QDBusReply<QString> mountpoint = filesystem.call("Mount", options);
		QString             errStr(mountpoint.error().message());

		if (not errStr.isEmpty())
		{
			qCritical() << "[QDBus Error]" << errStr;
			return false;
		}

		return true;
	}

	/* Mount this optical deivce */
	else
	{
		/* UDisks2 DBus API cannot mount an optical device (bug); use udisks command */
		QProcess proc;
		proc.start(QString("udisksctl"), QStringList() << "mount" << "-b" << mDevice);
		proc.waitForFinished();

		return(proc.exitCode() > 0 ? false : true);
	}
}


bool StorageBlock::unmount()
{
	if (not mIsOptical)
	{
		QDBusInterface filesystem(DBUS_SERVICE, mPath, QString("%1.Filesystem").arg(DBUS_SERVICE), QDBusConnection::systemBus());

		if (!filesystem.isValid())
		{
			qCritical() << QObject::tr("Failed D-Bus connection.");
			return false;
		}

		QDBusMessage reply = filesystem.call("Unmount", QVariantMap());

		QString errStr(reply.errorMessage());

		if (not errStr.isEmpty())
		{
			qCritical() << errStr;
			return false;
		}

		return true;
	}

	/* Unmount this optical device */
	else
	{
		/* UDisks2 DBus API cannot unmount an optical device (bug); use udisks command */
		QProcess proc;
		proc.start(QString("udisksctl"), QStringList() << "unmount" << "-b" << mDevice);
		proc.waitForFinished();

		return(proc.exitCode() > 0 ? false : true);
	}
}
