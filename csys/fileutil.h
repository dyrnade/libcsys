/*
  *
  * This file is a part of Libcsys.
  * Library for getting system resource information in real time.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  *
  */


#pragma once

#include "libcsys_global.h"

class LIBCSYSSHARED_EXPORT FileUtil {
public:
	static QString readStringFromFile(const QString& path, const QIODevice::OpenMode& mode             = QIODevice::ReadOnly);
	static QStringList readListFromFile(const QString& path, const QIODevice::OpenMode& mode           = QIODevice::ReadOnly);
	static bool writeFile(const QString& path, const QString& content, const QIODevice::OpenMode& mode = QIODevice::WriteOnly | QIODevice::Truncate);
};
