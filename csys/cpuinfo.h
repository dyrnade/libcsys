/*
  *
  * This file is a part of Libcsys.
  * Library for getting system resource information in real time.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  *
  */


#pragma once

#include "libcsys_global.h"

const QString PROC_CPUINFO = "/proc/cpuinfo";
const QString PROC_LOADAVG = "/proc/loadavg";
const QString PROC_STAT    = "/proc/stat";

class CpuTimes {
public:
	CpuTimes();
	CpuTimes(const QString &cpuTimes);
	qreal total();
	qreal idle();

private:
	qreal mTotal = 0;
	qreal mIdle  = 0;
};

/**
  * This class gives information about
  * current CPU usage, frequencies, and average loads.
  * CPU Usage: This will be the CPU usage since the last time
  * updateInfo() is called. Ideally, use a timer to call updateInfo()
  * every 500 ms or 1000 ms.
  * CPU Frequencies: Always the current frequencies will be returned.
  * CPU Load Avgs: Load avg of past 1m, 5m and 15m from as measured
  * when updateInfo() is called.
  */

class LIBCSYSSHARED_EXPORT CpuInfo {
public:
	/* Init */
	CpuInfo();

	/**
	  * Fetch the information from '/proc/<>' files.
	  * After calling the function we can get the new information by calling the getter methods.
	  */
	void updateInfo();

	/**
	  * Provides list of CPU frequencies in string.
	  * N.B. Before calling this function invoke updateInfo() function to get the latest information.
	  */
	QStringList getCPUFrequenciesStr() const;

	/**
	  * Provides list of CPU frequencies in double data type.
	  * N.B. Before calling this function invoke updateInfo() function to get the latest information.
	  */
	QList<qreal> getCPUFrequencies() const;

	/**
	  * Provides number of CPU Core.
	  * N.B. Before calling this function invoke updateInfo() function to get the latest information.
	  */
	quint8 getCpuCoreCount() const;

	/**
	  * Provides list of CPU percentages.
	  * N.B. Before calling this function invoke updateInfo() function to get the latest information.
	  */
	QList<int> getCpuPercents() const;

	/**
	  * Provides list of CPU core usages in average.
	  * N.B. Before calling this function invoke updateInfo() function to get the latest information.
	  */
	QList<double> getLoadAvgs() const;

	QString formatKHz(double value) const;

private:
	QStringList mCpuFreqsStr;
	QList<qreal> mCpuFreqs;
	QList<int> mCpuPercents;
	QList<double> mLoadAvgs = { 0, 0, 0 };

	QHash<int, CpuTimes> lastRead;
	QHash<int, CpuTimes> thisRead;
};
