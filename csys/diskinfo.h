/*
  *
  * This file is a part of Libcsys.
  * Library for getting system resource information in real time.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  *
  */


#pragma once

#include "libcsys_global.h"

const QString PROC_MOUNTS = "/proc/mounts";

class Disk;

class LIBCSYSSHARED_EXPORT DiskInfo {
public:
	QString getRootFolderDisk() const;
	QList<Disk> getDisks() const;
	void updateDiskInfo();
	QList<quint64> getDiskIO() const;
	QString getDiskName() const;
};

class Disk {
public:
	Disk();
	Disk(const Disk&);

	QString name() const;
	void setName(const QString& name);

	QString device() const;
	void setDevice(const QString& device);

	quint64 size() const;
	void setSize(const quint64& size);

	quint64 free() const;
	void setFree(const quint64& free);

	quint64 used() const;
	void setUsed(const quint64& used);

private:
	QString mName;
	QString mDevice;
	quint64 mSize;
	quint64 mFree;
	quint64 mUsed;
};
