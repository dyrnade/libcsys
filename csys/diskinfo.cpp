/*
  *
  * This file is a part of Libcsys.
  * Library for getting system resource information in real time.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, vsit http://www.gnu.org/licenses/.
  *
  */
#include <QtGlobal>

#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
#include <QRegularExpression>
#endif

#include <QStorageInfo>

#include "diskinfo.h"
#include "fileutil.h"


QString DiskInfo::getRootFolderDisk() const
{
	// Get disk name where root directory is located
	QFileInfo devPath(QStorageInfo::root().device());

	#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
	return devPath.baseName().remove(QRegularExpression("[0-9]+"));
	#else
	return devPath.baseName().remove(QRegExp("[0-9]+"));
	#endif
}

QList<Disk> DiskInfo::getDisks() const
{
	QList<Disk> lists;

	lists.clear();

	QList<QStorageInfo> storageInfoList = QStorageInfo::mountedVolumes();

	for (const QStorageInfo& info : storageInfoList)
	{
		Disk disk;
		disk.setName(info.displayName());
		disk.setDevice(info.device());
		disk.setSize(static_cast<quint64>(info.bytesTotal()));
		disk.setUsed(static_cast<quint64>(info.bytesTotal()) - info.bytesFree());
		disk.setFree(static_cast<quint64>(info.bytesFree()));

		lists << disk;
	}

	return lists;
}


void DiskInfo::updateDiskInfo()
{
	getDisks();
}


QList<quint64> DiskInfo::getDiskIO() const
{
	static QString diskName = getRootFolderDisk();//getDiskName();

	QList<quint64> diskReadWrite;

	#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
	QStringList diskStat = FileUtil::readStringFromFile(QString("/sys/block/%1/stat").arg(diskName))
							.trimmed()
							.split(QRegularExpression("\\s+"));
	#else
	QStringList diskStat = FileUtil::readStringFromFile(QString("/sys/block/%1/stat").arg(diskName))
							.trimmed()
							.split(QRegExp("\\s+"));
	#endif

	if (diskStat.count() > 7)
	{
		diskReadWrite.append(static_cast<unsigned long long>(diskStat.at(2).toLongLong()) * 512);
		diskReadWrite.append(static_cast<unsigned long long>(diskStat.at(6).toLongLong()) * 512);
	}

	return diskReadWrite;
}


QString DiskInfo::getDiskName() const
{
	QDir blocks("/sys/block");

	Q_FOREACH( const QFileInfo& entryInfo, blocks.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot))
	{
		if (QFile::exists(QString("%1/device").arg(entryInfo.absoluteFilePath())))
		{
			return entryInfo.baseName();
		}
	}

	return QString();
}


Disk::Disk()
	: mSize(0)
	, mFree(0)
	, mUsed(0)
{
}

Disk::Disk(const Disk &other)
{
	mName = other.mName;
	mDevice = other.mDevice;
	mSize = other.mSize;
	mFree = other.mFree;
	mUsed = other.mUsed;
}


QString Disk::name() const
{
	return mName;
}


void Disk::setName(const QString& name)
{
	mName = name;
}


QString Disk::device() const
{
	return mDevice;
}


void Disk::setDevice(const QString& device)
{
	mDevice = device;
}


quint64 Disk::size() const
{
	return mSize;
}


void Disk::setSize(const quint64& size)
{
	mSize = size;
}


quint64 Disk::free() const
{
	return mFree;
}


void Disk::setFree(const quint64& free)
{
	mFree = free;
}


quint64 Disk::used() const
{
	return mUsed;
}


void Disk::setUsed(const quint64& used)
{
	mUsed = used;
}
